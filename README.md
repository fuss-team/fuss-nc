# FUSS Nextcloud integration
## Spin your own Nextcloud instance and allow LDAP users to see their own homes


## Background

We assume you have a Proxmox Virtual Environment server
running a VM with Debian Jessie and a FUSS Server
configured, up and running.

![](images/starting_behaviour.png)


## Goal

The goal of this guide is to allow users of the network
to access their own home remotley via Nextcloud.

Nextcloud natively supports LDAP+Samba logon but there
are a few issues:
- you (probably) use a self-signed certificate to serve
  OpenLDAPs (`ldaps:///`);
- you do not own a public IP address;
- you need to protect the traffic between you and your
  (external) proxy.


## (New) background

Speaking in terms of distribution, Debian Jessie
it's too outdated to support the latest version of
Nextcloud. Docker could be a possibility but
the FUSS Server's firewall often conflicts with
Docker's iptables rules.

Definitely, the best solution is to have a second VM
running on the same Virtualization Server. The VM,
based on Debian 10 Buster, has to be connected to both
LAN and WAN. This will solve all firewall-related issues
as far as the Nextcloud server will be seen by the LDAP/Samba
server as a normal client.

![](images/final_behaviour.png)


## 1st step: create the new VM

The fastest way to have a Debian 10 VM up and running is to use
a dump of a template configured with cloud-init.

Ask the FUSS Team this file.

Copy your dump file to `/var/lib/vz/dump` then open your Proxmox VE
and go to the `local` storage, choose the `vzdump-` file you just copied
and click "Restore". Once filled out the form and confirmed the template
will be restored. This will take a few minutes.

At this time you have a template ready to be cloned. Choose the template
you just restored then select "More" > "Clone". Pick a name, leave the
preset as is and confirm.

The last step is to configure the newly created VM. As the template is
cloud-init ready you simply have to go under "Hardware" > "Add" >
"CloudInit drive". Add even the 2nd network interface then go under
the "Cloud-init" tab. Set `root` as User, a password and/or an
SSH key if you need it, a static IP for the WAN and the DHCP
for the LAN. Click "Regenerate image" and start the VM. Cloud-init will
configure and start it in a few seconds.

![](images/cloudinit.png)


## 2nd step: setup the VM

First of all, install some dependencies:

```
apt update
apt full-upgrade -y
apt install -y apache2 docker.io docker-compose
```

-WIP-

Clone then this repository

```
git clone https://gitlab.fuss.bz.it/fuss-team/fuss-nc.git
```

cd, and run

```
docker-compose up -d
```


## 3rd step: obtain a certificate

We assume you already asked the FUSS Team to create a domin for this
Nextcloud instance. You're now free to ask a certificate via

```
certbot certonly --webroot -w /var/www/html --server <THE ACME SERVER WE TOLD YOU TO USE> -d <YOUR DOMAIN> 
```


## 4rd step: configure Nextcloud

You can now securly login into your newly created Nextcloud instance.

-TODO: automatically enable LDAP and remoteFS-

-TODO: automatically copy LDAP key into the container-

Go to the settings and configure LDAP. Remember to use the FQDN of the
server and not the IP address.

Go then to the remote resource part, add a new one, put the FQDN of the server
and choose the "homes" share. Credentials: save in cache.
