#!/bin/bash
# Copyright (c) 2019 Marco Marinello <mmarinello@fuss.bz.it>
# Copyright (c) 2021 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <mmarinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Let /entrypoint.sh not run apache
tail -1 /entrypoint.sh | grep exec && sed -i '$ d' /entrypoint.sh

# Launch canonical entrypoint
/entrypoint.sh $@

# Function from the Nextcloud's original entrypoint
run_as() {
    if [ "$(id -u)" = 0 ]; then
        su -p www-data -s /bin/sh -c "$1"
    else
        sh -c "$1"
    fi
}

# Install CA certificate (now done via Dockerfile)
# mkdir -p /usr/local/share/ca-certificates/fuss.bz.it
# test -e /usr/local/share/ca-certificates/fuss.bz.it/fuss.crt || (
#   wget -O /usr/local/share/ca-certificates/fuss.bz.it/fuss.crt http://${ACME_SERVER}/ca.crt || true &&
#   update-ca-certificates || true
# )

# Configure certbot
if [ ! -e /etc/letsencrypt/accounts/10.0.101.50\:4001/ ]; then
  certbot register \
    --non-interactive \
    --agree-tos \
    --register-unsafely-without-email \
    --server https://${ACME_SERVER}:4001/acme/acme/directory
fi
certbot \
  --apache \
  --non-interactive \
  --server https://${ACME_SERVER}:4001/acme/acme/directory \
  -d ${NEXTCLOUD_INTERNAL_DOMAIN}


# Enable apps
run_as 'php /var/www/html/occ app:enable user_ldap'
run_as 'php /var/www/html/occ app:enable files_external'
# Should be set automatically if correctly working
# run_as 'php /var/www/html/occ background:cron'
run_as 'php /var/www/html/occ config:app:set theming enabled --value yes'
run_as 'php /var/www/html/occ config:app:set theming name --value "${INSTITUTE_NAME}"'
run_as 'php /var/www/html/occ config:app:set theming slogan --value "FUSS Remote Access"'
run_as 'php /var/www/html/occ config:app:set theming color --value "#996400"'
run_as 'php /var/www/html/occ config:app:set theming backgroundMime --value backgroundColor'
run_as 'php /var/www/html/occ config:app:set theming logoMime --value "image\/png"'
run_as 'php /var/www/html/occ config:app:set theming url --value "https://access.fuss.bz.it"'
run_as 'php /var/www/html/occ config:app:set theming privacyUrl --value "https://elisa.fuss.bz.it/privacy/gdpr_nextcloud_it_de.pdf"'
run_as 'php /var/www/html/occ config:app:set files default_quota --value 2MB'
run_as 'php /var/www/html/occ config:app:set privacy readableLocation --value it'
run_as 'cp /var/www/images/logo.png /var/www/html/core/img/logo/logo.png'
run_as 'cp /var/www/images/logo.png /var/www/html/core/img/logo/logo-mail.png'
run_as 'cp /var/www/images/logo.png /var/www/html/core/img/logo/logo-enterprise.png'
run_as 'cp /var/www/images/logo.svg /var/www/html/core/img/logo/logo.svg'
run_as 'php /var/www/html/occ maintenance:theme:update'

# Configure proxy
run_as 'php /var/www/html/occ config:system:set trusted_proxies 0 --value 10.0.101.75'
# New proxies
run_as 'php /var/www/html/occ config:system:set trusted_proxies 1 --value 10.0.101.70'
run_as 'php /var/www/html/occ config:system:set trusted_proxies 2 --value 10.0.101.71'
run_as "php /var/www/html/occ config:system:set overwritecondaddr --value ''"

# Default provided files to new users
run_as 'mkdir -p /var/www/skeleton'
run_as 'cp /var/www/images/logo.png /var/www/skeleton/FUSS.png'
run_as 'php /var/www/html/occ config:system:set skeletondirectory --value /var/www/skeleton'

# Check if ldap has to be configured
if [ -z "$(run_as 'php /var/www/html/occ ldap:show-config')" ]; then
  echo "Configure LDAP oAuth for FUSS Server"
  run_as 'php /var/www/html/occ ldap:create-empty-config'
  run_as "php /var/www/html/occ ldap:set-config s01 ldapHost 'ldaps://${FUSS_SERVER_FQDN}'"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapBase ${LDAP_BASE_DN}"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapBaseGroups ${LDAP_BASE_DN}"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapBaseUsers ${LDAP_BASE_DN}"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGidNumber gidNumber"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupDisplayName cn"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupFilter '(&(|(objectclass=posixGroup))(|(cn=nextcloud)))'"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupFilterGroups 'nextcloud'"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupFilterMode 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapLoginFilter '(&(|(objectclass=posixAccount))(uid=%uid))'"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupFilterObjectclass posixGroup"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapLoginFilterEmail 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapLoginFilterMode 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapLoginFilterUsername 1"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapNestedGroups 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapGroupMemberAssocAttr memberUid"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapPort 636"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapTLS 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUserAvatarRule default"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUserDisplayName gecos"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUserFilter '(|(objectclass=posixAccount))'"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUserFilterMode 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUserFilterObjectclass posixAccount"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUuidGroupAttribute auto"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapUuidUserAttribute auto"
  run_as "php /var/www/html/occ ldap:set-config s01 turnOffCertCheck 1"
  run_as "php /var/www/html/occ ldap:set-config s01 turnOnPasswordChange 0"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapConfigurationActive 1"
  run_as "php /var/www/html/occ ldap:set-config s01 ldapExpertUsernameAttr uid"
fi

# Check if remote share needs to be configured
if [ "No admin mounts configured" = "$(run_as 'php /var/www/html/occ files_external:list')" ]; then
  echo "Configure remote homes for FUSS Server"
  cat > /tmp/fe_conf.txt <<EOF
{
    "mount_id": 3,
    "mount_point": "\/",
    "storage": "\\\\OCA\\\\Files_External\\\\Lib\\\\Storage\\\\SMB",
    "authentication_type": "password::logincredentials",
    "configuration": {
        "host": "${FUSS_SERVER_FQDN}",
        "share": "homes",
        "root": "",
        "domain": "",
        "show_hidden": false
    },
    "options": {
        "enable_sharing": true,
        "filesystem_check_changes": 1,
        "previews": true,
        "readonly": false
    },
    "applicable_users": [],
    "applicable_groups": [
        "${FUSS_AUTHORIZED_GROUP}"
    ]
}
EOF
  run_as "php /var/www/html/occ files_external:import /tmp/fe_conf.txt"
  rm /tmp/fe_conf.txt
fi

# Install and configure the FUSS Online Collaboration Suite
# at the moment we're unable to reach github assets so we
# need to use a our own mirror
# rm -rf /var/www/html/apps/richdocuments
# wget -O /tmp/richdocuments.tar.gz https://access.fuss.bz.it/static/nc_cdn/apps/richdocuments.tar.gz
# tar -C /var/www/html/apps -xvzf /tmp/richdocuments.tar.gz
# run_as 'php /var/www/html/occ app:enable richdocuments'
run_as 'php /var/www/html/occ app:install richdocuments'
run_as 'php /var/www/html/occ config:app:set richdocuments enabled --value yes'
run_as 'php /var/www/html/occ config:app:set richdocuments wopi_url --value "https://onlinesuite.fuss.bz.it"'
run_as 'php /var/www/html/occ config:app:set richdocuments public_wopi_url --value "https://onlinesuite.fuss.bz.it"'
run_as 'php /var/www/html/occ richdocuments:activate-config'

# DB Fixes
run_as 'php /var/www/html/occ db:add-missing-indices'
run_as 'php /var/www/html/occ db:convert-filecache-bigint'

# Start cron
/cron.sh &

# Kill server started by certbot
killall -9 apache2

# Run the server
exec "$@"
