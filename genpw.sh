#!/bin/bash

echo -n GEN PWS ...

echo -n > .env

for i in $(cat example.env); do
	if echo $i | grep PASSWORD > /dev/null; then
		echo $i | cut -d '=' -f 1 | tr -d '\n' >> .env
		echo -n = >> .env
		pwgen 30 1 >> .env
	else
		echo $i >> .env
	fi
done

echo OK
